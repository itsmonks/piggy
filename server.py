from flask import Flask
from pygame import mixer
from random import seed
from random import randint

# make numbers random
seed(1)

file_mappings = {
    1: "night.mp3",
    2: "ohyeah.mp3",
    3: "moan.mp3",
    4: "numnum.mp3"
}

kid_friendly = False

def get_sound():
    random_number = randint(1,3) if not kid_friendly else 4
    return file_mappings[random_number]

app = Flask(__name__)
@app.route('/')
def index():
    mixer.init()
    mixer.music.load('sounds/' + get_sound() )
    mixer.music.play()
    return 'played sound'

if __name__ == '__main__':
    app.run(debug=False, port=8000, host='0.0.0.0')

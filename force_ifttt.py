from selenium import webdriver
import time

options = webdriver.ChromeOptions()
# options.add_argument('headless')
options.add_argument('--ignore-certificate-errors')
options.add_argument('--test-type')
options.add_argument('--start-maximized')
options.binary_location = 'C:\Program Files (x86)\Google\Chrome\Application\chrome.exe'
driver = webdriver.Chrome(options=options)

driver.get('https://ifttt.com/login?wp_=1')
email = driver.find_element_by_id('user_username')
email.send_keys('')

pwd = driver.find_element_by_id('user_password')
pwd.send_keys('')

pwd.submit()

driver.get('https://ifttt.com/applets/hN798kC3')
check = driver.find_element_by_link_text('Check now')
while True:
    print("Forcing check")
    check.click()
    time.sleep(0.5)

driver.close()
